#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 13 20:29:19 2021

@author: juds
"""

#%% Import libraries

import pandas as pd
pd.options.display.max_columns = 50
import numpy as np
import seaborn as sns
sns.set()
from datetime import datetime
from datetime import date
import multiprocessing
from random import seed
from random import random

#%% Load data

data_presence_path = "../datasets/Presence_17773_mayo_junio_2020.csv"
data_zones_path = "../datasets/zonas_17773_mayo_ junio_2020.csv"
data_presence = pd.read_csv(data_presence_path, header = 0)
data_zones = pd.read_csv(data_zones_path, header = 0)

#%% EDA data_presence

data_presence.start = pd.to_datetime(data_presence.start)
data_presence.end = pd.to_datetime(data_presence.end)

data_zones.start = pd.to_datetime(data_zones.start)
data_zones.end = pd.to_datetime(data_zones.end)

#%% Configurando fechas: fecha inicial y la fecha final

date_min = min(data_presence['start'])
date_max = max(data_presence['start'])

#%% 1

from datetime import timedelta

initial_date = date_min
initial_year, initial_month, initial_day = initial_date.year,\
                                        initial_date.month, initial_date.day

last_date = date_max
last_year, last_month, last_day, last_hour, last_min, last_sec =\
    last_date.year, last_date.month, last_date.day,\
    last_date.hour, last_date.minute, last_date.second

initial_time = datetime(initial_year, initial_month, initial_day)\
                                            - timedelta(seconds = 1)
last_time = datetime(last_year, last_month, last_day, last_hour, last_min, last_sec)

date_list = [initial_time + timedelta(days = i)
             for i in range((last_time - initial_time).days + 1)]

#%% Definir el rango de fecha para analizar junto con los segundos para la
    # preferencia de clientes

for i in range(0, len(date_list) - 1):
#for i in range(14, 16):
    date_ini, date_last, seconds = date_list[i], date_list[i + 1], 50000
    # Generación de información para presence y zones:
    # data_presence
    data_date_presence = data_presence[(data_presence['start'] > date_ini) &\
                          (data_presence['start'] < date_last)]
    data_date_presence.sort_values(by = ['client_mac', 'start'],
                      inplace = True,
                      ascending = False)
    # 7
    # Se eliminan las columnas 'timestamp', 'end_time_utc' y 'start_time_utc' ya
    # que son independientes de las horas de registro entrada/salida.
    data_date_presence.drop(['timestamp', 'start_time_utc', 'end_time_utc'],
                            axis = 1,
                            inplace = True)
    # 8
    # Se concluye de 'data_date_presence' que los valores con duración 0 no tienen
    # razón en ser incluidos en la tabla, por lo tanto se deden eliminar.
    # También se eliminan duplicados.
    data_date_presence = data_date_presence[\
                                (data_date_presence['duration'] != 0) &\
                                (data_date_presence['duration'] != 1)]
    data_date_presence = data_date_presence.drop_duplicates()
    # 9
    # data_zones
    data_date_zones = data_zones[(data_zones['start'] > date_ini) &\
                          (data_zones['start'] < date_last)]
    data_date_zones.sort_values(by = ['mac_zones', 'start'],
                      inplace = True,
                      ascending = False)
    # 10
    # Se descubre que hay zonas con misma hora de entrada y salida, por lo que
    # son registros duplicados, se eliminan. Previamente se debe eliminar la columna
    # 'last_update' y 'hour'  ya que son independientes de las horas de registro
    # entrada/salida.
    data_date_zones.drop(['last_update', 'hour'], axis = 1, inplace = True)
    data_date_zones = data_date_zones[(data_date_zones['duration'] != 0) &\
                                  (data_date_zones['duration'] != 1)]
    data_date_zones = data_date_zones.drop_duplicates()
    # Para la fecha en particular dada 
    # Zonas con mayor y menor asistencia por los clientes
    preference_data_date_zones = data_date_zones\
                            .groupby(data_date_zones['name'])['duration']\
                                .sum()\
                                    .sort_values(ascending = False)
    preference_data_date_zones = preference_data_date_zones.reset_index()
    # Clientes ("preferentes") con mayor tiempo en sucursal
    preference_data_date_presence = data_date_presence\
            .groupby(data_date_presence['client_mac'])['duration']\
                .sum()\
                    .sort_values(ascending = False)
    preference_data_date_presence = preference_data_date_presence.reset_index()
    # 11
    preference_data_date_presence_copy = preference_data_date_presence.values.tolist()
    cols_ddp = list(preference_data_date_presence.columns)
    def client_format(row):
        lista_c =[row[0][-10:]]
        lista_r = [row[l] for l in range(1, len(row))]
        lista_c.extend(lista_r)
        return lista_c
    pool = multiprocessing.Pool()
    preference_data_date_presence_copy_m =\
        pd.DataFrame(pool.map(client_format,
                              preference_data_date_presence_copy),
                     columns = cols_ddp)
    preference_data_date_presence =\
        preference_data_date_presence_copy_m.copy()
    # 3.0 Creación de variables
    # Los dfs importantes son:
    # data_date_presence, data_date_zones, preference_data_date_presence,\
    # preference_data_date_zones, preference_data_date_zones_SRJR
    # Se conservan los df's de las fechas selecionadas
    preference_presence = preference_data_date_presence.copy()
    preference_zones = preference_data_date_zones.copy()
    # 14
    preference_presence_list = preference_presence.iloc[:, 1].values # vector 
    preference_zones_list = preference_zones.iloc[:, 1].values # vector 
    # 15
    time = 10*60
    preference_presence_list =\
        preference_presence[preference_presence['duration'] > time].iloc[:, :].values # vector 
    preference_zones_list =\
        preference_zones[preference_zones['duration'] > time].iloc[:, :].values # vector 
    # 16
    len_pzl = len(preference_zones_list)
    len_ppl = len(preference_presence_list)
    if len_pzl == 0 or len_ppl == 0:
            continue
    cols_ppla = list(preference_presence_list[:, 0])
    cols_pzla = list(preference_zones_list[:, 0])
    # 17
    iterac = 1000
    randoms = []
    confianza = 0.90
    preference_presence_coef = []
    iterglob = 10
    iterglob2 = 10
    # 18
    de_nuevo = 0
    for o in range(iterglob):
        seed(o + 400)
        preference_zones_list_arr = preference_zones_list[:, 1]/len_ppl
        randoms = []
        preference_presence_coef = []
        # Transform data by function np.log1p
        preference_presence_list_arr =\
            np.log1p(list(preference_presence_list[:, 1]/len_pzl))*540.
        for l in range(iterglob2):
            preference_presence_coef = []
            for arr in preference_presence_list_arr:
                for z in range(iterac):
                    the_end = 'not ok'
                    for x in range(len(preference_zones_list_arr)):
                        array = np.array([random()\
                            for y in range(len(preference_zones_list_arr))])
                        comblineal = preference_zones_list_arr*array
                        sum_combLineal = np.sum(comblineal)
                        if sum_combLineal >= arr*confianza and\
                            sum_combLineal <= arr:
                                the_end = 'ok'
                                preference_presence_coef.append(list(comblineal))
                                break
                    if the_end == 'ok':
                        break
                #if the_end == 'not ok':
                #    print('No se halló la combinación para el elemento: ', arr, 'aumentar iterac.')
                #    break
                # Disminución de confianza si no se encuentra la combinación con la
                # confianza inicial
                if z == iterac - 1 and the_end == 'not ok':
                    for confi in [0.85, 0.8]:
                        for aa in range(iterac):
                            the_end = 'not ok'
                            for bb in range(len(preference_zones_list_arr)):
                                array = np.array([random()\
                                for y in range(len(preference_zones_list_arr))])
                                comblineal = preference_zones_list_arr*array
                                sum_combLineal = np.sum(comblineal)
                                if sum_combLineal >= arr*confi and\
                                    sum_combLineal <= arr:
                                        the_end = 'ok'
                                        preference_presence_coef.append(list(comblineal))
                                        break
                            if the_end == 'ok':
                                break
                        if the_end == 'ok':
                            break
                        if the_end == 'not ok':
                            array = np.array([random()\
                            for y in range(len(preference_zones_list_arr))])
                            comblineal = preference_zones_list_arr*array
                            sum_combLineal = np.sum(comblineal)
                            preference_presence_coef.append(list(comblineal))
                            break 
            if x == len(preference_zones_list_arr) - 1:
                conf_zones = 0.6
                preference_presence_coef = np.array(preference_presence_coef)
                cols_ppc = np.apply_along_axis(sum, 0, preference_presence_coef)*2.
                for w in range(len(preference_zones_list_arr)):
                    if cols_ppc[w] >= conf_zones*float(preference_zones_list[w][1]) and \
                        cols_ppc[w] < preference_zones_list[w][1]*1.05 :
                            de_nuevo = 'No de nuevo'
                            continue
                    else:
                        de_nuevo = 'De nuevo'
                        break
            elif de_nuevo == 'No de nuevo':
                break
        if de_nuevo == 'No de nuevo':
            print('=========================================================== \n \
                  Se ha encontrado la combinación en la itaración iterglob: ', o, '\n',
                '%s-%s-%s' % (date_last.day, date_last.month, date_last.year))
            break
        print('No se ha encontrado la combinación total. Iteración: ', o)
    # 19
    array = np.array(preference_presence_coef)
    sum_array_by_zonas = np.apply_along_axis(sum, 0, array)*2.
    sum_array_by_client = np.apply_along_axis(sum, 1, array)
    cols_pzla.append('duration_client')
    # Creando la tabla de tiempos por zonas de los clientes
    T_client = preference_presence_list[:, 1]
    array_ppc = np.insert(array, array.shape[1],
                      sum_array_by_client,
                      axis = 1)
    for u in range(len(array_ppc)):
        array_ppc[u] = (array_ppc[u]/array_ppc[u][-1])*T_client[u]
    time_zones = np.apply_along_axis(sum, 0, array_ppc) # Se corroboran los tiempo de las zonas!!
    time_client = np.apply_along_axis(sum, 1, array_ppc[:, 0:-1]) # Se corroboran los tiempo de los clientes!!
    table_zonas_time_clients = pd.DataFrame(array_ppc, columns = cols_pzla).transpose()
    table_zonas_time_clients.columns = [col for col in cols_ppla]
    # Uniendo las tablas de table_zonas_time_clients y clientes
    df_zonas_time_clients = table_zonas_time_clients.transpose().reset_index()
    df_zonas_time_clients.rename(columns = {'index': 'client_mac10'}, inplace = True)
    df_zonas_time_clients = df_zonas_time_clients.drop(['duration_client'], axis = 1)
    data_date_presence2 = data_presence[(data_presence['start'] > date_ini) &\
                          (data_presence['start'] < date_last)]
    df_clientes_New = data_date_presence2.drop(['timestamp', 'start_time_utc',
                'end_time_utc', 'rssi_min', 'rssi_max', 'client_mac_hashed', 'user_id',
                'mobile_validated', 'first_seen', 'last_seen', 'visits',
                'year', 'month', 'day'], axis = 1)\
            .drop_duplicates(subset = ['client_mac'])
    df_clientes_New['client_mac10'] = [df_clientes_New.iloc[i][0][-10:] for i in range(len(df_clientes_New))]
    df_clientes_zonas_time = df_clientes_New\
                    .merge(df_zonas_time_clients, how = 'inner', on = 'client_mac10')\
                        .fillna(0).drop(['duration', 'start', 'end', 'vendor',
                                         'venue_id'], axis = 1)
    # Añadiendo la columna de edad
    df_clientes_zonas_time_birth = df_clientes_zonas_time[df_clientes_zonas_time['date_of_birth'] != 0]
    df_clientes_zonas_time_birth.date_of_birth = pd.to_datetime(df_clientes_zonas_time_birth.date_of_birth)
    today = pd.to_datetime(date.today())
    age = []
    for i in range(len(df_clientes_zonas_time_birth)):
        ed = (today - df_clientes_zonas_time_birth.iloc[i]['date_of_birth']).days/365
        if ed > 100:
            ed = ed - 100
        age.append(int(ed))
    df_clientes_zonas_time_birth['age'] = age
    # Save table in csv file
    df_clientes_zonas_time_birth.to_csv('../output_data/clientes_with_age_and_time_%s_%s_%s.csv'\
                        % (date_last.day, date_last.month, date_last.year),
                        index = True)
    # Crando la tabla de rankings
    table_rankings = table_zonas_time_clients.transpose().drop('duration_client', axis = 1)
    table_rankings_arr = np.array(table_rankings)
    # 20
    # En esta parte se añade un ranking  de preferencia de 4 si es su zona favorita,
    # 3 si le gusta mucho, 2 si le agrada y 1 curiosidad, 0 si simplemente pasó por esta zona
    # Se proponen los siguientes rangos (se pueden modificar de acuerdo a las
    # criterios del marketing) para categorizar las zonas de acuerdo a la
    # preferencia: 4 si >= 8000, 3 si >= 2000 and < 8000, 2 si < 2000 and >= 1000,
    # 1 si < 1000 and >= 300 y 0 si <300
    for v in range(len(table_rankings)):
        for t in range(len(table_rankings.iloc[0, :])):
            x = table_rankings.iloc[v, t]
            if x >= 8000:
                table_rankings.iloc[v, t] = 4
            elif x >= 2000 and x < 8000:
                table_rankings.iloc[v, t] = 3
            elif x >= 1000 and x < 2000:
                table_rankings.iloc[v, t] = 2
            elif x >= 300 and x < 1000:
                table_rankings.iloc[v, t] = 1
            else:
                table_rankings.iloc[v, t] = 0
    # Save table in csv file
    table_rankings.to_csv('../output_data/table_rankings_%s_%s_%s.csv'\
                        % (date_last.day, date_last.month, date_last.year),
                        index = True)
    # Uniendo las tablas de table_rankings y clientes
    df_zonas_R_client = table_rankings.reset_index()
    df_zonas_R_client.rename(columns = {'index': 'client_mac10'}, inplace = True)
    data_date_presence2 = data_presence[(data_presence['start'] > date_ini) &\
                          (data_presence['start'] < date_last)]
    df_clientes_New = data_date_presence2.drop(['timestamp', 'start_time_utc',
                'end_time_utc', 'rssi_min', 'rssi_max', 'client_mac_hashed', 'user_id',
                'mobile_validated', 'first_seen', 'last_seen', 'visits',
                'year', 'month', 'day'], axis = 1)\
        .drop_duplicates(subset = ['client_mac'])
    df_clientes_New['client_mac10'] = [df_clientes_New.iloc[p][0][-10:] for p in range(len(df_clientes_New))]
    df_clientes_zonas = df_clientes_New\
                    .merge(df_zonas_R_client, how = 'inner', on = 'client_mac10')\
                        .fillna(0).drop(['duration', 'start', 'end', 'vendor',
                                         'venue_id'], axis = 1)
    # Añadiendo la columna de edad
    df_clientes_zonas_birth = df_clientes_zonas[df_clientes_zonas['date_of_birth'] != 0]
    df_clientes_zonas_birth.date_of_birth = pd.to_datetime(df_clientes_zonas_birth.date_of_birth)
    today = pd.to_datetime(date.today())
    age = []
    for r in range(len(df_clientes_zonas_birth)):
        ed = (today - df_clientes_zonas_birth.iloc[r]['date_of_birth']).days/365
        if ed > 100:
            ed = ed - 100
        age.append(int(ed))
    df_clientes_zonas_birth['age'] = age
    # Save table in csv file
    df_clientes_zonas_birth.to_csv('../output_data/table_clientes_with_age_%s_%s_%s.csv'\
                        % (date_last.day, date_last.month, date_last.year),
                        index = False)
    df_clientes_zonas = df_clientes_zonas\
                    .merge(df_clientes_zonas_birth[['client_mac10', 'age']],
                           how = 'left',
                           on = 'client_mac10')
    # Save table in csv file
    df_clientes_zonas.fillna(0).to_csv('../output_data/table_clientes_zonas_%s_%s_%s.csv'\
                        % (date_last.day, date_last.month, date_last.year),
                        index = False)


