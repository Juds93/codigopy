#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 10 12:11:30 2021

@author: juds
"""


# Import libraries
import pandas as pd
pd.options.display.max_columns = 50
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
from datetime import datetime
import multiprocessing
import os

#%% 1

# Load data
data_presence_path = "../datasets/Presence_17773_mayo_junio_2020.csv"
data_zones_path = "../datasets/zonas_17773_mayo_ junio_2020.csv"
data_presence = pd.read_csv(data_presence_path, header = 0)
data_zones = pd.read_csv(data_zones_path, header = 0)

#%% EDA data_presence

data_presence.shape

head_data_presence = data_presence.head()
describe_data_presence = data_presence.describe()

data_presence.start = pd.to_datetime(data_presence.start)
data_presence.end = pd.to_datetime(data_presence.end)

cols_data_presence = list(data_presence.columns)
data_presence.info()

#%% Configurando fechas: fecha inicial y la fecha final

date_min = min(data_presence['start'])
date_max = max(data_presence['start'])

#%% 2

from datetime import timedelta

initial_date = date_min
initial_year, initial_month, initial_day = initial_date.year,\
                                        initial_date.month, initial_date.day

last_date = date_max
last_year, last_month, last_day, last_hour, last_min, last_sec =\
    last_date.year, last_date.month, last_date.day,\
    last_date.hour, last_date.minute, last_date.second

initial_time = datetime(initial_year, initial_month, initial_day)\
                                            - timedelta(seconds = 1)
last_time = datetime(last_year, last_month, last_day, last_hour, last_min, last_sec)

date_list = [initial_time + timedelta(days = i)
             for i in range((last_time - initial_time).days + 1)]


#%% Definir el rango de fecha para analizar junto con los segundos para la
    # preferencia de clientes

date_ini, date_last, seconds = date_list[0], date_list[1], 40000

#%% Aquí se calculan las visitas por día de los clientes en la tienda 

clients = []
for i in range(len(date_list) - 1):
    clients.append(pd.DataFrame((data_presence['client_mac']\
    [(data_presence['start'] > date_list[i]) &\
     (data_presence['start'] < date_list[i+1])]\
        .value_counts()\
            .index.values,\
                np.array(data_presence['client_mac']\
                         [(data_presence['start'] >  date_list[i]) &\
                          (data_presence['start'] < date_list[i+1])]\
                             .value_counts()\
                                 .tolist()
                        )\
                                )\
                                )\
                   .transpose()\
                       .rename(columns={0: 'client_mac', 1: 'start'})\
                           .set_index('client_mac')
                    )

#%% 3

df_clients = pd.concat([df for df in clients], axis = 1).fillna(0).transpose()

head_df_clients = df_clients.head()

columns_df_clients = list(df_clients.columns)
columns_df_clients = [columns_df_clients[i][-10:] for i in range(len(columns_df_clients))]

print(columns_df_clients[0], type(columns_df_clients[0]))

df_clients.columns = columns_df_clients

df_clients.head(1)

df_clients.set_index([pd.Index(['day%s' %(i + 1) for i in range(len(df_clients))])], inplace = True)

df_clients.head()

cols_clients = list(df_clients.columns)

#%% 4

wd = os.getcwd()

path_image_client = wd  + '/images/cliente_'

for i in range(len(df_clients.iloc[0, :])):
    fig, axes = plt.subplots(1, 2, sharey = True, figsize = (15,8))
    fig.suptitle('Comportamiento temporal de clientes por día y su estadística total, periodo %s - %s'\
              % (date_min, date_max))
        
    g = sns.lineplot(ax = axes[0], data = df_clients[cols_clients[i]])
    plt.setp(g.get_xticklabels(), rotation = 90)
    axes[0].set_ylabel('Cliente: ' + '%s' % cols_clients[i])
    axes[0].set_xlabel('Días')
    axes[0].set_title('Vísitas del cliente por día');
    
    sns.boxplot(ax = axes[1], data = df_clients[cols_clients[i]])
    plt.setp(g.get_xticklabels(), rotation = 90)
    axes[1].set_title('Estadística general del cliente');
    axes[1].set_xlabel('Cliente: ' + '%s' % cols_clients[i])
    axes[1].set(xticklabels=[])
    plt.savefig(path_image_client + '%s' % cols_clients[i] + '.png',
                dpi = 200,
                format = 'png')
    plt.close()
    
    if i == 20: # Se pueden aumentar el número de clientes para visualizar su comportamiento temporal
        break

#%% EDA data_zones

data_zones.shape

data_zones.head()

data_zones.start = pd.to_datetime(data_zones.start)
data_zones.end = pd.to_datetime(data_zones.end)

cols_data_zones = list(data_zones.columns)
data_zones.info()

#%% 5

zones = []
for i in range(len(date_list) - 1):
    zones.append(pd.DataFrame((data_zones['name']\
                                 [(data_zones['start'] >  date_list[i]) &\
                                  (data_zones['start'] < date_list[i+1])]\
                                 .value_counts().index.values,
                                 np.array(data_zones['name']\
                                          [(data_zones['start'] >  date_list[i]) &\
                                           (data_zones['start'] < date_list[i+1])]\
                                          .value_counts().tolist())))\
                   .transpose()\
                   .rename(columns={0: 'name', 1: 'start'})\
                   .set_index('name'))
        
#%% 6

df_zones = pd.concat([df for df in zones], axis = 1).fillna(0).transpose()

df_zones.head()

columns_df_zones = list(df_zones.columns)
columns_df_zones = [columns_df_zones[i] for i in range(len(columns_df_zones))]
#print(columns_df_zones[0], type(columns_df_zones[0]))

df_zones.columns = columns_df_zones

df_zones.head(1)

df_zones.set_index([pd.Index(['day%s' %(i + 1)for i in range(len(df_zones))])],
                   inplace = True)

df_zones.head()

df_zones.rename(columns = {'Relojería / Joyería': 'Relojería-Joyería'},
                inplace = True)
cols_zones = list(df_zones.columns)

#%% 7
wd = os.getcwd()

path_image_zone = wd + '/images/zona_'

for i in range(len(df_zones.iloc[0, :])):
    fig, axes = plt.subplots(1, 2, sharey = True, figsize = (15,8))
    fig.suptitle('Número de visitas en la zona por día y estadística total, periodo %s - %s'\
              % (date_min, date_max))
        
    g = sns.lineplot(ax = axes[0], data = df_zones[cols_zones[i]])
    plt.setp(g.get_xticklabels(), rotation = 90)
    axes[0].set_ylabel('Zona: ' + '%s' % cols_zones[i])
    axes[0].set_xlabel('Días')
    axes[0].set_title('Vísitas en la zona por día');
    
    sns.boxplot(ax = axes[1], data = df_zones[cols_zones[i]])
    plt.setp(g.get_xticklabels(), rotation = 90)
    axes[1].set_title('Estadística general de la zona')
    axes[1].set_xlabel('Zona: ' + '%s' % cols_zones[i])
    axes[1].set(xticklabels=[])
    
    plt.savefig(path_image_zone + '%s' % cols_zones[i] + '.png',
                dpi = 200,
                format = 'png')
    plt.close()

#%% Generación de información para presence y zones:

# data_presence

data_date_presence = data_presence[(data_presence['start'] > date_ini) &\
                          (data_presence['start'] < date_last)]

data_date_presence.sort_values(by = ['client_mac', 'start'],
                      inplace = True,
                      ascending = False)

#%% 8

# Se eliminan las columnas 'timestamp', 'end_time_utc' y 'start_time_utc' ya
# que son independientes de las horas de registro entrada/salida.

data_date_presence.drop(['timestamp', 'start_time_utc', 'end_time_utc'],
                            axis = 1,
                            inplace = True)

#%% 9

# Se concluye de 'data_date_presence' que los valores con duración 0 no tienen
# razón en ser incluidos en la tabla, por lo tanto se deden eliminar.
# También se eliminan duplicados.

data_date_presence = data_date_presence[data_date_presence['duration'] != 0]

data_date_presence = data_date_presence.drop_duplicates()

#%% 10
    
# data_zones

data_date_zones = data_zones[(data_zones['start'] > date_ini) &\
                          (data_zones['start'] < date_last)]

data_date_zones.sort_values(by = ['mac_zones', 'start'],
                      inplace = True,
                      ascending = False)
data_date_zones.head(20)

#%% 11

# Se descubre que hay zonas con misma hora de entrada y salida, por lo que
# son registros duplicados, se eliminan. Previamente se debe eliminar la columna
# 'last_update' y 'hour'  ya que son independientes de las horas de registro
# entrada/salida.

data_date_zones.drop(['last_update', 'hour'], axis = 1, inplace = True)

data_date_zones = data_date_zones[data_date_zones['duration'] != 0]

data_date_zones = data_date_zones.drop_duplicates()

#%% Para la fecha en particular dada 

# Zonas con mayor y menor asistencia por los clientes

preference_data_date_zones = data_date_zones\
                            .groupby(data_date_zones['name'])['duration']\
                                .sum()\
                                    .sort_values(ascending = False)
                                    
preference_data_date_zones = preference_data_date_zones.reset_index()

wd = os.getcwd()

path_zones_rel = wd + '/images/zonas_rel'

# Set the figure size 
fig = plt.figure(1, [20, 8]) 
ax = sns.catplot(x = "name", y = "duration", hue = "duration",
               data = preference_data_date_zones,
               height = 6,
               aspect = 2,
               s = 10) 

for axes in ax.axes.flat:
    _ = axes.set_xticklabels(axes.get_xticklabels(), rotation = 50)
    
plt.title('Zonas más recurrentes de la fecha del %s al %s' % (date_ini, date_last))
plt.savefig(path_zones_rel + '.png',
            bbox_inches = 'tight',
                dpi = 150,
                format = 'png')
plt.close()

#%% 12
    
# Clientes ("preferentes") con mayor tiempo en sucursal

preference_data_date_presence = data_date_presence\
            .groupby(data_date_presence['client_mac'])['duration']\
                .sum()\
                    .sort_values(ascending = False)
                                    
preference_data_date_presence = preference_data_date_presence.reset_index()

#%% 13

preference_data_date_presence_copy = preference_data_date_presence.values.tolist()

cols_ddp = list(preference_data_date_presence.columns)

def client_format(row):
    lista_c =[row[0][-10:]]
    lista_r = [row[i] for i in range(1, len(row))]
    lista_c.extend(lista_r)
    return lista_c

pool = multiprocessing.Pool()
preference_data_date_presence_copy_m =\
    pd.DataFrame(pool.map(client_format, preference_data_date_presence_copy),
                 columns = cols_ddp)

preference_data_date_presence = preference_data_date_presence_copy_m.copy()

#%% 14

# 'seconds' para identificar clientes con más tiempo en la sucursal y llevar a
# cabo campañas para ellos por su fidelidad (propuesta)

wd = os.getcwd()

path_client_rel = wd + '/images/client_rel'

# Set the figure size 
fig = plt.figure(1, [20, 8]) 
ax = sns.catplot(x = "client_mac", y = "duration", hue = "duration",
               data = preference_data_date_presence[
                        preference_data_date_presence['duration'] > seconds],
               height = 6,
               aspect = 2,
               s = 10) 

for axes in ax.axes.flat:
    _ = axes.set_xticklabels(axes.get_xticklabels(), rotation = 50)

plt.title('Clientes con más tiempo total de visita de la fecha del %s al %s' % (date_ini, date_last))
plt.savefig(path_client_rel + '.png',
            bbox_inches = 'tight',
                dpi = 150,
                format = 'png')
plt.close()

#%% Comportamiento de zonas con mayor y menor relevancia para los clientes

# Sin Relojería/Joyería ni Restaurante

preference_data_date_zones_SRJR = preference_data_date_zones.drop([0, 1])
    # 0, 1 'Relojería / Joyería', 'Restaurante' respectivamente

#%% 15

wd = os.getcwd()

path_zonas_rel_srjr = wd + '/images/zonas_rel_srjr'

# Set the figure size
fig = plt.figure(1, [20, 8])
ax = sns.catplot(x = "name", y = "duration", hue = "duration",
               data = preference_data_date_zones_SRJR,
               height = 6,
               aspect = 2,
               s = 10)

for axes in ax.axes.flat:
    _ = axes.set_xticklabels(axes.get_xticklabels(), rotation = 50)

plt.title('Zonas con mayor tiempo de visita en la fecha del %s al %s' % (date_ini, date_last))
plt.savefig(path_zonas_rel_srjr + '.png',
            bbox_inches = 'tight',
                dpi = 150,
                format = 'png')
plt.close()

# In[]: 3.0 Creación de variables

# Se conservan los df's de las fechas selecionadas
preference_presence = preference_data_date_presence.copy()
preference_zones = preference_data_date_zones.copy()

head_pp = preference_presence.head()
head_pz = preference_zones.head()

#%% 14

from random import seed
from random import random

preference_presence_list = preference_presence.iloc[:, 1].values # vector 
preference_zones_list = preference_zones.iloc[:, 1].values # vector 

#%% 16

time = 5*60

preference_presence_list =\
    preference_presence[preference_presence['duration'] > time].iloc[:, :].values # vector 
preference_zones_list =\
    preference_zones[preference_zones['duration'] > time].iloc[:, :].values # vector 

#%% 17

len_pzl = len(preference_zones_list)
len_ppl = len(preference_presence_list)

cols_ppla = list(preference_presence_list[:, 0])
cols_pzla = list(preference_zones_list[:, 0])

#%% 18

iterac = 1000
randoms = []
confianza = 0.90
preference_presence_coef = []
iterglob = 10
iterglob2 = 10

#%% Aquí se resulve el sistema de ecuaciones. Hay hiperparámetros que pueden
# cambiar con respecto al día que se analiza

de_nuevo = 0

for o in range(iterglob):
    seed(o + 400)
    preference_zones_list_arr = preference_zones_list[:, 1]/len_ppl
    randoms = []
    preference_presence_coef = []
    # Transform data by function np.log1p
    preference_presence_list_arr =\
        np.log1p(list(preference_presence_list[:, 1]/len_pzl))*600
    for l in range(iterglob2):
        #seed(l + 1350)
        preference_presence_coef = []
        for arr in preference_presence_list_arr:
            for i in range(iterac):
                the_end = 'not ok'
                for j in range(len(preference_zones_list_arr)):
                    #n = n + 1
                    array = np.array([random() for k in range(len(preference_zones_list_arr))])
                    #array[0: 2] = array[0: 2]
                    comblineal = preference_zones_list_arr*array
                    sum_combLineal = np.sum(comblineal)
                    if sum_combLineal >= arr*confianza and\
                        sum_combLineal <= arr:
                            the_end = 'ok'
                            preference_presence_coef.append(list(comblineal))
                            break
                if the_end == 'ok':
                    break
            if the_end == 'not ok':
                print('No se halló la combinaicón para el elemento: ', arr, 'aumentar iterac.')
                break
        if j == len(preference_zones_list_arr) - 1:
            conf_zones = 0.85
            preference_presence_coef = np.array(preference_presence_coef)
            cols_ppc = np.apply_along_axis(sum, 0, preference_presence_coef)*2.
            for k in range(len(preference_zones_list_arr)):
                if cols_ppc[k] >= conf_zones*float(preference_zones_list[k][1]) and \
                    cols_ppc[k] < preference_zones_list[k][1]*1.05 :
                        de_nuevo = 'No de nuevo'
                        continue
                else:
                    de_nuevo = 'De nuevo'
                    break
        elif de_nuevo == 'No de nuevo':
            break
    if de_nuevo == 'No de nuevo':
        print('Se ha encontrado la combinación en la itaración iterglob: ', o)
        break
    print('No se ha encontrado la combinación total. Iteración: ', o)

#%% 19

array = np.array(preference_presence_coef)

sum_array_by_zonas = np.apply_along_axis(sum, 0, array)*2.
sum_array_by_client = np.apply_along_axis(sum, 1, array)

cols_pzla.append('duration_client')

#%% Creando la tabla de tiempos por zonas de los clientes

T_client = preference_presence_list[:, 1]

array_ppc = np.insert(array, array.shape[1],
                      sum_array_by_client,
                      axis = 1)

for i in range(len(array_ppc)):
    array_ppc[i] = (array_ppc[i]/array_ppc[i][-1])*T_client[i]

time_zones = np.apply_along_axis(sum, 0, array_ppc) # Se corroboran los tiempo de las zonas!!
time_client = np.apply_along_axis(sum, 1, array_ppc[:, 0:-1]) # Se corroboran los tiempo de los clientes!!

table_zonas_time_clients = pd.DataFrame(array_ppc, columns = cols_pzla).transpose()
table_zonas_time_clients.columns = [col for col in cols_ppla]

# Save table in csv file

table_zonas_time_clients.to_csv('../output_data/zones_time_clients.csv', index = True)

#%% Gráficos de los tiempos por zona de los clientes

wd = os.getcwd()

path_zonas_client = wd + '/images/zonas_client_'

for j in range(len(table_zonas_time_clients.iloc[0, :])):
    if j == 20 - 1: # Se puede aumentar el número de clientes para visualizar su comportamiento temporal
        break
    fig = plt.figure(1, [20, 8])
    ax = sns.pointplot(x = table_zonas_time_clients\
                     .drop('duration_client', axis = 0)\
                         .index,
                     y = cols_ppla[j],
                     data = table_zonas_time_clients\
                         .drop('duration_client', axis = 0),
                     hue = cols_ppla[j],
                     height = 6,
                     aspect = 2,
                     s = 30,
                     palette='dark'
                     )
    plt.xticks(rotation = 40)
    plt.title('Comportamiento del cliente en diferentes zonas')
    
    plt.savefig(path_zonas_client + cols_ppla[j] + '.png',
            bbox_inches = 'tight',
                dpi = 150,
                format = 'png')
    plt.close()

#%% Crando la tabla de rankings

table_rankings = table_zonas_time_clients.transpose().drop('duration_client', axis = 1)
table_rankings_arr = np.array(table_rankings)

#%% 20

# En esta parte se añade (siguiendo la idea previa de proyecto) un ranking de
# preferencia de 4 si es su zona favorita, 3 si le gusta mucho, 2 si le agrada
# y 1 curiosidad, 0 si simplemente pasó por esta zona. Se proponen los siguientes
# rangos (se pueden modificar de acuerdo a las criterios del negocio) para
# categorizar las zonas de acuerdo a la preferencia: 4 si >= 8000,
# 3 si >= 2000 and < 8000, 2 si < 2000 and >= 1000, 1 si < 1000 and >= 300
# y 0 si <300

for i in range(len(table_rankings)):
    for j in range(len(table_rankings.iloc[0, :])):
        x = table_rankings.iloc[i, j]
        if x >= 8000:
            table_rankings.iloc[i, j] = 4
        elif x >= 2000 and x < 8000:
            table_rankings.iloc[i, j] = 3
        elif x >= 1000 and x < 2000:
            table_rankings.iloc[i, j] = 2
        elif x >= 300 and x < 1000:
            table_rankings.iloc[i, j] = 1
        else:
            table_rankings.iloc[i, j] = 0

# Save table in csv file

table_rankings.to_csv('../output_data/table_rankings.csv', index = True)

#%% Uniendo las tablas de table_rankings y clientes

df_zonas_R_client = table_rankings.reset_index()
df_zonas_R_client.rename(columns = {'index': 'client_mac10'}, inplace = True)

data_date_presence2 = data_presence[(data_presence['start'] > date_ini) &\
                          (data_presence['start'] < date_last)]

df_clientes_New = data_date_presence2.drop(['timestamp', 'start_time_utc',
                'end_time_utc', 'rssi_min', 'rssi_max', 'client_mac_hashed', 'user_id',
                'mobile_validated', 'first_seen', 'last_seen', 'visits',
                'year', 'month', 'day'], axis = 1)\
    .drop_duplicates(subset = ['client_mac'])

df_clientes_New['client_mac10'] = [df_clientes_New.iloc[i][0][-10:] for i in range(len(df_clientes_New))]

df_clientes_zonas = df_clientes_New\
                    .merge(df_zonas_R_client, how = 'inner', on = 'client_mac10')\
                        .fillna(0).drop(['duration', 'start', 'end', 'vendor',
                                         'venue_id'], axis = 1)

#%% Añadiendo la columna de edad

from datetime import date

df_clientes_zonas_birth = df_clientes_zonas[df_clientes_zonas['date_of_birth'] != 0]
df_clientes_zonas_birth.date_of_birth = pd.to_datetime(df_clientes_zonas_birth.date_of_birth)

today = pd.to_datetime(date.today())

age = []
for i in range(len(df_clientes_zonas_birth)):
    ed = (today - df_clientes_zonas_birth.iloc[i]['date_of_birth']).days/365
    if ed > 100:
        ed = ed - 100
    age.append(int(ed))

df_clientes_zonas_birth['age'] = age

# Save table in csv file

df_clientes_zonas_birth.to_csv('../output_data/table_clientes_with_age.csv', index = False)

#%%

df_clientes_zonas = df_clientes_zonas\
                    .merge(df_clientes_zonas_birth[['client_mac10', 'age']],
                           how = 'left',
                           on = 'client_mac10')

# Save table in csv file

df_clientes_zonas.fillna(0).to_csv('../output_data/table_clientes_zonas.csv', index = False)


#%%


