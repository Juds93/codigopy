#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 14 13:21:50 2021

@author: juds
"""

#%% Import libraries

import pandas as pd
pd.options.display.max_columns = 50
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
from datetime import datetime
import os
wd = os.getcwd()

#%% Load data

data_zones_path = "../datasets/zonas_17773_mayo_ junio_2020.csv"
data_zones = pd.read_csv(data_zones_path, header = 0)

#%% EDA data_zones 
data_zones.shape

data_zones.head()

data_zones.start = pd.to_datetime(data_zones.start)
data_zones.end = pd.to_datetime(data_zones.end)

data_zones['name'][data_zones['name'] == 'Relojería / Joyería'] = 'Relojería_Joyería'

cols_data_zones = list(data_zones.columns)
data_zones.info()

cols_zones = list(data_zones['name'].unique())
cols_zones.sort()


data_zones[['year', 'month', 'day']] =\
    data_zones[['year', 'month', 'day']].astype('str')
    
data_zones['date_day'] = data_zones['year'] + '-' +\
    data_zones['month'] + '-' + data_zones['day']

date_of = datetime(2020, 5, 1, 0, 0, 0)

data_zones = data_zones[data_zones['start'] >= date_of]

#%% Configurando fechas: fecha inicial y la fecha final

date_min = min(data_zones['start'])
date_max = max(data_zones['start'])

#%% 1

from datetime import timedelta

initial_date = date_min
initial_year, initial_month, initial_day = initial_date.year,\
                                        initial_date.month, initial_date.day

last_date = date_max
last_year, last_month, last_day, last_hour, last_min, last_sec =\
    last_date.year, last_date.month, last_date.day,\
    last_date.hour, last_date.minute, last_date.second

initial_time = datetime(initial_year, initial_month, initial_day)\
                                            - timedelta(seconds = 1)
last_time = datetime(last_year, last_month, last_day, last_hour, last_min, last_sec)

date_list = [initial_time + timedelta(days = i)
             for i in range((last_time - initial_time).days + 2)]

#%% Aquí se calculan las visitas por día en las zonas de la tienda 

zones = []

for i in range(len(date_list) - 1):
    zones.append([date_list[i + 1], pd.DataFrame((data_zones['name']\
                                 [(data_zones['start'] >  date_list[i]) &\
                                  (data_zones['start'] < date_list[i + 1])]\
                                 .value_counts().index.values,
                                 np.array(data_zones['name']\
                                          [(data_zones['start'] >  date_list[i]) &\
                                           (data_zones['start'] < date_list[i + 1])]\
                                          .value_counts().tolist())))\
                   .transpose()\
                   .rename(columns={0: 'name', 1: 'start'})\
                   .set_index('name')])

#%% 5

days_zones = [zones[i][0] for i in range(len(zones))]

import calendar

days_z = [
    calendar.day_name[days_zones[i].weekday()]
    for i in range(len(days_zones))
    ]

for i in range(len(days_z)):
    if days_z[i] == 'Sunday':
        days_z[i] = 'Domingo'
    elif days_z[i] == 'Monday':
        days_z[i] = 'Lunes'
    elif days_z[i] == 'Tuesday':
        days_z[i] = 'Martes'
    elif days_z[i] == 'Wednesday':
        days_z[i] = 'Miércoles'
    elif days_z[i] == 'Thursday':
        days_z[i] = 'Jueves'
    elif days_z[i] == 'Friday':
        days_z[i] = 'Viernes'
    elif days_z[i] == 'Saturday':
        days_z[i] = 'Sábado'

#%%

df_zones = pd.concat([zones[i][1].transpose() for i in range(len(zones))],
                     axis = 0).fillna(0)
df_zones['días_semana'] = days_z
df_zones['fecha_día'] =[str(date_list[i + 1].day) + '-'\
                        + str(date_list[i + 1].month) + '-'\
                            + str(date_list[i + 1].year) + '-'\
                                + df_zones.iloc[i, -1]\
                                    for i in range(len(date_list) - 1)]

df_zones['fecha'] =[str(date_list[i + 1].day) + '-'\
                        + str(date_list[i + 1].month) + '-'\
                            + str(date_list[i + 1].year)\
                                for i in range(len(date_list) - 1)]
df_zones['fecha'] = pd.to_datetime(df_zones.fecha, format = '%d-%m-%Y')


df_zones.drop(['días_semana'], axis = 1, inplace = True)

df_zones = df_zones.set_index('fecha_día')

#%%

n = 0
m = 0
font = {'family': 'serif',
        'color':  'black',
        'weight': 'normal',
        'size': 24,
        }

#zonas_images = wd + '/images/zonas_images_'

for j in range(0, len(cols_zones) - 2):
    df_zones.iloc[:, j: j + 3].plot(kind = 'bar',
                                    stacked = False,
                                    width = 1.,
                                    figsize = (24, 12))
    plt.legend(loc = 'best')#, bbox_to_anchor = (1.005, 1))
    plt.xticks(rotation = 60)
    plt.ylabel('Número de visitas', fontdict = font)
    plt.xlabel('Días de la semana', fontdict = font)
    plt.title('Comparativa entre zonas: %s-%s-%s'\
                % (cols_zones[j], cols_zones[j + 1], cols_zones[j + 2]),
                fontdict = font)
#    plt.savefig(zonas_images + '%s-%s-%s'\
#                % (cols_zones[j], cols_zones[j + 1], cols_zones[j + 2]) + '.png',
#                dpi = 200,
#                format = 'png')
    #plt.close()
    plt.show()

#%%

df_zones['días_semana'] = days_z
df_zones['month'] = [date_list[i].month for i in range(1, len(date_list))]
df_zones.to_csv('../output_data/csv_zones_graphics/table_df_zones.csv', index = False)
