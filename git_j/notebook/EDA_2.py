#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 20 20:44:21 2021

@author: juds
"""

#%% Import libraries

import pandas as pd
pd.options.display.max_columns = 50
pd.options.display.max_rows = 50
import numpy as np
from matplotlib import pyplot as plt
import seaborn as sns
sns.set()
import os
wd = os.getcwd()

#%% Load data

data_presence_path = "../datasets/Presence_17773_mayo_junio_2020.csv"
data_zones_path = "../datasets/zonas_17773_mayo_ junio_2020.csv"

data_presence = pd.read_csv(data_presence_path, header = 0)
data_zones = pd.read_csv(data_zones_path, header = 0)

#%% EDA data_presence

data_presence.start = pd.to_datetime(data_presence.start)
data_presence.end = pd.to_datetime(data_presence.end)

data_zones.start = pd.to_datetime(data_zones.start)
data_zones.end = pd.to_datetime(data_zones.end)

#%% Fecha inicial, fecha final

date_min = min(data_presence['start'])
date_max = max(data_presence['start'])

#%% initial info data

data_presence.info()

#%%

# =============================================================================
# cols_data_presence = list(data_presence.columns)
# =============================================================================

cols_drop = ['timestamp', 'start_time_utc', 'end_time_utc', 'rssi_min',\
             'rssi_max', 'vendor', 'client_mac_hashed', 'mobile_validated',\
                 'twitter_id', 'instagram_id', 'linkedin_id', 'oneclick_id']
    
data_presence = data_presence.drop(cols_drop, axis = True)

# =============================================================================
# cols_data_presence = list(data_presence.columns)
# =============================================================================

#%% Tranforming column gender and age

data_presence.fillna(0, inplace = True)
data_presence = data_presence.sort_values('start')

data_presence['gender'][(data_presence['gender'] != 'F') &\
                        (data_presence['gender'] != 'M')] = 'Unknown'

head_data_presence = data_presence.head(50)

        
#%% Añadiendo el campo edad

from datetime import date

today = pd.to_datetime(date.today())

data_presence_birth = data_presence[data_presence['date_of_birth'] != 0]
data_presence_birth.date_of_birth =\
    pd.to_datetime(data_presence_birth.date_of_birth)

age = []

for i in range(len(data_presence_birth)):
    ed = (today - data_presence_birth.iloc[i]['date_of_birth']).days/365
    if ed > 100:
        ed = ed - 100
    age.append(int(ed))

data_presence_birth['age'] = age

data_presence_birth = data_presence_birth[data_presence_birth['age'] > 15]

#%% Conversión de fechas a días

days_presence_birth = [data_presence_birth.iloc[i][2]\
                       for i in range(len(data_presence_birth))]

import calendar

days_z = [
    calendar.day_name[days_presence_birth[i].weekday()]
    for i in range(len(days_presence_birth))
    ]

for i in range(len(days_z)):
    if days_z[i] == 'Sunday':
        days_z[i] = 'Domingo'
    elif days_z[i] == 'Monday':
        days_z[i] = 'Lunes'
    elif days_z[i] == 'Tuesday':
        days_z[i] = 'Martes'
    elif days_z[i] == 'Wednesday':
        days_z[i] = 'Miércoles'
    elif days_z[i] == 'Thursday':
        days_z[i] = 'Jueves'
    elif days_z[i] == 'Friday':
        days_z[i] = 'Viernes'
    elif days_z[i] == 'Saturday':
        days_z[i] = 'Sábado'

data_presence_birth['dias'] = days_z

#%% Generando fechas por días

data_presence_birth[['year', 'month', 'day']] =\
    data_presence_birth[['year', 'month', 'day']].astype('str')

data_presence_birth['date_day'] = data_presence_birth['year'] + '-' +\
    data_presence_birth['month'] + '-' + data_presence_birth['day']

#%% COMIENZAN LOS GRÁFICOS

freq_month = data_presence_birth[['month', 'visits']].groupby(['month'])\
                    .sum().reset_index()

meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',\
         'Noviembre', 'Diciembre']

for i in range(len(freq_month)):
    if freq_month.iloc[i, 0] == '1':
        freq_month.iloc[i, 0] = meses[0]
    elif freq_month.iloc[i, 0] == '2':
        freq_month.iloc[i, 0] = meses[1]
    elif freq_month.iloc[i, 0] == '3':
        freq_month.iloc[i, 0] = meses[2]
    elif freq_month.iloc[i, 0] == '4':
        freq_month.iloc[i, 0] = meses[3]
    elif freq_month.iloc[i, 0] == '5':
        freq_month.iloc[i, 0] = meses[4]
    elif freq_month.iloc[i, 0] == '6':
        freq_month.iloc[i, 0] = meses[5]
    elif freq_month.iloc[i, 0] == '7':
        freq_month.iloc[i, 0] = meses[6]
    elif freq_month.iloc[i, 0] == '8':
        freq_month.iloc[i, 0] = meses[7]
    elif freq_month.iloc[i, 0] == '9':
        freq_month.iloc[i, 0] = meses[8]
    elif freq_month.iloc[i, 0] == '10':
        freq_month.iloc[i, 0] = meses[9]
    elif freq_month.iloc[i, 0] == '11':
        freq_month.iloc[i, 0] = meses[10]
    elif freq_month.iloc[i, 0] == '12':
        freq_month.iloc[i, 0] = meses[11]
freq_month

#%% Visitas totales por mes

path_visitas_totales_mes = wd + '/images/visitas_totales_mes'

fontsize = 18

plt.figure(figsize = (9, 7))
sns.barplot(x = 'month', y = 'visits',
            data = freq_month,
            ci = None)
plt.xticks(rotation = 60, fontsize = fontsize)
plt.xlabel('Mes', fontsize = fontsize)
plt.ylabel('Total de visitas', fontsize = fontsize)
plt.title('Gráfica general de visitas totales por mes \n Periodo %s - %s'\
          % (str(date_min.year) + '-' + str(date_min.month)+ '-' + str(date_min.day), str(date_max.year)\
             + '-' + str(date_max.month)+ '-' + str(date_max.day)), fontsize = fontsize, fontweight = 'bold')
# =============================================================================
# plt.savefig(path_visitas_totales_mes + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
plt.show()

# Save file csv

freq_month.to_csv('../output_data/csv_EDA_2/table_freq_month.csv', index = False)

#%% Gráfica de la probabilidad de la edad del cliente que visita la tienda

path_Probabilidad_edad_cliente = wd + '/images/Probabilidad_edad_cliente'
fontsize = 18

plt.figure(figsize = (7, 5))
sns.histplot(data = data_presence_birth, x="age",
             stat = "probability",
             discrete=True,
             hue = 'gender',
             kde = True)
plt.xlabel('Edad', fontsize = fontsize)
plt.ylabel('Probabilidad', fontsize = fontsize)
plt.title('Probabilidad de la edad del cliente que puede visitar la tienda', fontsize = fontsize, fontweight = 'bold')
# =============================================================================
# plt.savefig(path_Probabilidad_edad_cliente + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
plt.show()

# Save file csv

data_presence_birth.to_csv('../output_data/csv_EDA_2/table_data_presence_birth.csv', index = False)


#%%  Gŕafica general de la frecuencia de visitas en función de la edad

freq_visit_age = data_presence_birth[['age', 'visits', 'gender']]\
    .groupby(['gender', 'age']).sum().reset_index()
#freq_visit_age.T

fontsize = 18

path_Freq_visit_vs_edad = wd + '/images/Freq_visit_vs_edad'

plt.figure(figsize = (7, 5))
sns.lineplot(x = 'age', y = 'visits', hue = 'gender',
                 lw = 1.5, estimator = None,
                 data = freq_visit_age
            )
plt.xticks(rotation = 60, fontsize = fontsize)
plt.xlabel('Edad', fontsize = fontsize)
plt.ylabel('Total de visitas', fontsize = fontsize)
plt.title('Gŕafica general de la frecuencia de visitas en función de la edad \n Periodo %s - %s'\
          % (str(date_min.year) + '-' + str(date_min.month)+ '-' + str(date_min.day), str(date_max.year)\
             + '-' + str(date_max.month)+ '-' + str(date_max.day)), fontsize = fontsize, fontweight='bold')
# =============================================================================
# plt.savefig(path_Freq_visit_vs_edad + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
plt.show()

# Save file csv

freq_visit_age.to_csv('../output_data/csv_EDA_2/table_freq_visit_age.csv', index = False)

#%% Group by days and counts visits

freq_days = data_presence_birth[['date_day', 'visits']]\
    .groupby(['date_day'])\
        .sum()\
            .reset_index()

#%% Conversión de fechas a días de la semana

freq_days_date = [freq_days.iloc[i][0] for i in range(len(freq_days))]

days_z = [
    calendar.day_name[pd.to_datetime(freq_days_date[i]).weekday()]
    for i in range(len(freq_days_date))
    ]

for i in range(len(days_z)):
    if days_z[i] == 'Sunday':
        days_z[i] = 'Domingo'
    elif days_z[i] == 'Monday':
        days_z[i] = 'Lunes'
    elif days_z[i] == 'Tuesday':
        days_z[i] = 'Martes'
    elif days_z[i] == 'Wednesday':
        days_z[i] = 'Miércoles'
    elif days_z[i] == 'Thursday':
        days_z[i] = 'Jueves'
    elif days_z[i] == 'Friday':
        days_z[i] = 'Viernes'
    elif days_z[i] == 'Saturday':
        days_z[i] = 'Sábado'

freq_days['dias'] = days_z

#%% Gráfica general visitas totales en los días de la semanaa

fontsize = 18
freq_days_plot = freq_days[['dias', 'visits']].groupby(['dias']).sum().reset_index()

path_visitas_totales_semana = wd + '/images/visitas_totales_semana'

plt.figure(figsize = (9, 7))
sns.barplot(x = 'dias', y = 'visits',
            data = freq_days_plot,
            ci = None)
plt.xticks(rotation = 60, fontsize = fontsize)
plt.xlabel('Días de la semana', fontsize = fontsize)
plt.ylabel('Total de visitas', fontsize = fontsize)
plt.title('Gráfica general de visitas totales en los días de la semana \n Periodo %s - %s'\
          % (str(date_min.year) + '-' + str(date_min.month)+ '-' + str(date_min.day),\
             str(date_max.year) + '-' + str(date_max.month)+ '-' + str(date_max.day)),\
         fontweight='bold', fontsize = fontsize)
# =============================================================================
# plt.savefig(path_visitas_totales_semana + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
plt.show()

# Save file csv

freq_days_plot.to_csv('../output_data/csv_EDA_2/table_freq_days_plot.csv', index = False)

#%% Gráfica por días, se comparan los mismos días

dias = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']

path_visitas_dias_similares = wd + '/images/visitas_dias_similares_'
fontsize = 18

for dia in dias:
    plt.figure(figsize = (9, 7))
    sns.barplot(x = 'date_day', y = 'visits',
            data = freq_days[freq_days['dias'] == dia])
    plt.xticks(rotation = 60, fontsize = fontsize)
    plt.title('Comparación de visitas de los mismos días \n Periodo %s - %s'\
          % (str(date_min.year) + '-' + str(date_min.month)+ '-' + str(date_min.day), str(date_max.year) + '-' + str(date_max.month)+ '-' + str(date_max.day))
            + ' día %s' % dia, fontweight='bold', fontsize = fontsize)
    plt.ylabel('Número de visitas', fontsize = fontsize)
    plt.xlabel('Fecha', fontsize = fontsize)
# =============================================================================
#     plt.savefig(path_visitas_dias_similares + '%s' % dia  + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
    plt.show()

# Save file csv

freq_days.to_csv('../output_data/csv_EDA_2/table_freq_days.csv', index = False)

#%% Reindexando las filas mediante el formateo de fecha de 'freq_days'

freq_days['fecha'] = pd.to_datetime(freq_days['date_day'])
freq_days.sort_values('fecha', inplace = True)
freq_days.drop(['fecha'], axis = 1, inplace = True)
freq_days.reset_index(drop = True, inplace = True)

#%% Gráficas de comparación por semana

n = 0
m = 0
path_visitas_por_semana = wd + '/images/visitas_por_semana'
fontsize = 18

for i in range(len(freq_days)):
    n = n + 1
    if freq_days.iloc[i, 2] == 'Domingo':
        plt.figure(figsize = (9, 7))
        sns.barplot(x = 'dias', y = 'visits',
            data = freq_days.iloc[m: n, :])
        plt.xticks(rotation = 60, fontsize = fontsize)
        plt.title('Gráfica de comparación de visitas totales por semana \n Periodo %s a %s'\
                  % (freq_days.iloc[m, 0], freq_days.iloc[n - 1, 0]), fontsize = fontsize, fontweight='bold')
        plt.xlabel('Días', fontsize = fontsize)
        plt.ylabel('Número de visitas', fontsize = fontsize)
# =============================================================================
#         plt.savefig(path_visitas_por_semana + '%s' % freq_days.iloc[m, 0] + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
        plt.show()
        m = n
    if i == len(freq_days) - 1 and freq_days.iloc[m - 1, 2] == 'Domingo':
        plt.figure(figsize = (9, 7))
        sns.barplot(x = 'dias', y = 'visits',
            data = freq_days.iloc[m: , :])
        plt.xticks(rotation = 60, fontsize = fontsize)
        plt.xlabel('Días', fontsize = fontsize)
        plt.ylabel('Número de visitas', fontsize = fontsize)
        plt.title('Gráfica de comparación de visitas totales por semana \n Periodo %s a %s'\
                  % (freq_days.iloc[m, 0], freq_days.iloc[n - 1, 0]), fontsize = fontsize, fontweight='bold')
# =============================================================================
#         plt.savefig(path_visitas_por_semana + '%s' % freq_days.iloc[m, 0] + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
        plt.show()

#%% Gráfica por días, se comparan los mismos días con hue

path_Comparación_frecuencia_visitas = wd + '/images/Comparación_frecuencia_visitas'

fontsize = 18

for dia in dias:
    plt.figure(figsize = (9, 7))
    sns.barplot(x = 'date_day', y = 'visits', hue = 'gender',
            data = data_presence_birth[data_presence_birth['dias'] == dia],
            estimator = sum,
            ci = None)
    plt.xticks(rotation = 60, fontsize = fontsize)
    plt.xlabel('Días', fontsize = fontsize)
    plt.ylabel('Total de visitas', fontsize = fontsize)
    plt.title('Comparación de la frecuencia de visitas por género \n Días %s' % dia, fontsize = fontsize, fontweight='bold')
# =============================================================================
#     plt.savefig(path_Comparación_frecuencia_visitas + '%s' % dia + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
    plt.show()
    
#%% Comportamiento general de las visitas en la tienda en todo el periodo

path_Comportamiento_general_visitas = wd + '/images/Comportamiento_general_visitas'

plt.figure(figsize = (30, 18))
sns.barplot(data = data_presence_birth,
            x = 'date_day',
            y = 'visits',
            ci = None,
            estimator = sum)
plt.xticks(rotation = 60, fontsize = 25)
plt.yticks(fontsize = 25)
plt.xlabel('Día', fontsize = 30)
plt.ylabel('Visitas', fontsize = 30)
plt.title('Comportamiento general de las visitas en la tienda en todo el periodo',\
          fontsize = 40, fontweight='bold')
# =============================================================================
# plt.savefig(path_Comportamiento_general_visitas + '%s' % dia + '.png',
#                         dpi = 200,
#                         format = 'png')
# =============================================================================
plt.show()

#%%


















